public class Rectangle{
    private double length;
    private double width;

    public Rectangle(double length, double width){
        this.length = length;
        this.width = width;
    }

    public double getLength(){
        return this.length;
    }

    public double getWidth(){
        return this.width;
    }

    public double getPerimeter(){
        return 2*(this.length+this.width);
    }

    public double getArea(){
        return this.length*this.width;
    }

    @Override
    public String toString(){
        return "Length: "+this.length+
        "\n" + "Width: "+this.width+
        "\n" + "Perimeter: "+this.getPerimeter()+
        "\n" + "Area: "+this.getArea();
    }
}
