import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SquareTests {
/*
    // mySquare test will fail
    @Test
    public void test() {
        fail("mySquare test will fail");
    }

    @Test
    // mySquare test will succeed. In practice we would put
    // a real method call into the assertEquals
    public void test2() {
        assertEquals(1,1);
    }
*/
    @Test
    public void testSide(){
        Square mySquare = new Square(4);
        assertEquals(4, mySquare.getSide());
    }

    @Test
    public void testArea(){
        Square mySquare = new Square(4);
        assertEquals(16, mySquare.getArea());
    }

    @Test
    public void testString(){
        Square mySquare = new Square(4);
        assertEquals("the side is: " + 4.0 + ", the perimeter is: " + 16.0 + ", the area is: " + 16.0, mySquare.toString());
    }
}