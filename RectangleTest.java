import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class RectangleTest {

    @Test
    public void ConstructorTest(){
        Rectangle r = new Rectangle(5, 7);
    }

    @Test
    public void AreaTest() {
        Rectangle r = new Rectangle(4, 8);
        assertEquals(32, r.getArea());
    }

    @Test
    public void toStringTest(){
        Rectangle r = new Rectangle(5, 7);
        assertEquals("Length: "+5.0+
        "\n" + "Width: "+7.0+
        "\n" + "Perimeter: "+24.0+
        "\n" + "Area: "+35.0, r.toString());
    }

}