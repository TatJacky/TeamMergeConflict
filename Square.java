public class Square {
    private double side;

    public Square(double side){
        this.side = side;
    }

    public double getSide(){
        return this.side;
    }

    public double getPerimeter(){
        return this.side*4;
    }

    public double getArea(){
        return this.side*this.side;
    }

    public String toString(){
        return "the side is: " + this.side + ", the perimeter is: " + this.getPerimeter() + ", the area is: " + this.getArea();
    }
}
